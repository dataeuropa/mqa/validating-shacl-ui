const config = {
  title: 'SHACL Validator - data.europa.eu',
  description: 'SHACL Validator - data.europa.eu',
  link: 'https://www.data.europa.eu',
  shaclLevels: [
    {
      id: 0,
      name: 'DCAT-AP 3.0.0 - SHACL Shapes Level 1',
      technicalModel: 'dcatap300level1'
    },
    {
      id: 1,
      name: 'DCAT-AP 3.0.0 - SHACL Shapes Level 2',
      technicalModel: 'dcatap300level2'
    },
    {
      id: 3,
      name: 'DCAT-AP 3.0.0 - SHACL Shapes Level 3',
      technicalModel: 'dcatap300level3'
    },
    {
      id: 4,
      name: 'DCAT-AP 3.0.0 - Complete SHACL Shapes',
      technicalModel: 'dcatap300'
    },
    {
      id: 5,
      name: 'DCAT-AP 2.1.1 - SHACL Shapes Level 1',
      technicalModel: 'dcatap211level1'
    },
    {
      id: 6,
      name: 'DCAT-AP 2.1.1 - SHACL Shapes Level 2',
      technicalModel: 'dcatap211level2'
    },
    {
      id: 7,
      name: 'DCAT-AP 2.1.1 - SHACL Shapes Level 3',
      technicalModel: 'dcatap211level3'
    },
    {
      id: 8,
      name: 'DCAT-AP 2.1.1 - Complete SHACL Shapes',
      technicalModel: 'dcatap211'
    },
    {
      id: 9,
      name: 'DCAT-AP 2.1.0 - SHACL Shapes Level 1',
      technicalModel: 'dcatap210level1'
    },
    {
      id: 10,
      name: 'DCAT-AP 2.1.0 - Complete SHACL Shapes',
      technicalModel: 'dcatap210'
    },
    {
      id: 11,
      name: 'DCAT-AP 2.0.1',
      technicalModel: 'dcatap201'
    },
    {
      id: 12,
      name: 'DCAT-AP 2.0.0',
      technicalModel: 'dcatap200'
    },
    {
      id: 13,
      name: 'DCAT-AP 2.0',
      technicalModel: 'dcatapde20'
    },
    {
      id: 14,
      name: 'DCAT-AP 2.0 conventions',
      technicalModel: 'dcatapde20conventions'
    },
    {
      id: 15,
      name: 'DCAT-AP 1.2.1',
      technicalModel: 'dcatap121'
    },
    {
      id: 16,
      name: 'DCAT-AP 1.2.1 (without patches)',
      technicalModel: 'dcatap121orig'
    },
    {
      id: 17,
      name: 'DCAT-AP 1.2',
      technicalModel: 'dcatap12'
    },
    {
      id: 18,
      name: 'DCAT-AP 1.1',
      technicalModel: 'dcatap11'
    },
    {
      id: 19,
      name: 'DCAT-AP 1.1 (prior hotfixes)',
      technicalModel: 'dcatap11orig'
    },
    {
      id: 20,
      name: 'DCAT-AP 1.0.2',
      technicalModel: 'dcatapde102'
    },
  ]
};

export { config };

