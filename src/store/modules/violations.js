import Vue from 'vue';
import axios from 'axios';
import { getDetailedNetworkErrorMsg } from '@/utils/helpers';

function getShaclViolations(body, type, shapeModel) {
  const shaclApi = axios.create({
    baseURL: Vue.prototype.$env.VUE_APP_SHACL_API_URL || process.env.VUE_APP_SHACL_API_URL || 'http://localhost:8000',
    headers: {
      Accept: '*/*',
      'Content-Type': `${type}`,
    },
    params: {
      shapeModel,
    }
    
  });
  return shaclApi.post('validation/report', body);
}

function getShaclViolationsByURL(shaclUrl, type, shapeModel) {
  return getShaclViolations(shaclUrl, type, shapeModel);
}

function getShaclViolationsBySchema(schema, type, shapeModel) {
  return getShaclViolations(schema, type, shapeModel);
}

export default {
  namespaced: true,
  state: {
    violations: [],
    requestError: null,
    violationsSuccess: false,
    isLoading: false,
    violationsContext: null,
    shapeModel: null
  },

  getters: {
    getViolations: (state) => state.violations,
    getViolationsContext: (state) => state.violationsContext,
    getViolationsSuccess: (state) => state.violationsSuccess,
    getLoading: (state) => state.isLoading,
    getRequestError: (state) => state.requestError,
    getShapeModel: (state) => state.shapeModel
  },

  actions: {
    async getViolations({ commit }, { body, bySchema = true, type, shapeModel}) {
      // Reset violations
      commit('SET_VIOLATIONS', []);
      commit('SET_VIOLATIONS_SUCCESS', true);
      commit('SET_REQUEST_ERROR', null);
      commit('SET_CONTEXT', null);
      commit('SET_LOADING', true);
      try {
        // Get violations
        const response = bySchema
          ? await getShaclViolationsBySchema(body, type, shapeModel)
          : await getShaclViolationsByURL(body, type, shapeModel);

        // Artifically delay response to test loading behavior
        const apiDelay = Vue.prototype.$env.VUE_APP_DEV_API_REQUEST_DELAY
          || process.env.VUE_APP_DEV_API_REQUEST_DELAY
          || 0;
        if (process.env.NODE_ENV !== 'production' && apiDelay) {
          // Make sure this line is never executed in production
          await new Promise((resolve) => setTimeout(resolve, apiDelay));
        }

        if (response.data['@graph']) commit('SET_VIOLATIONS', response.data['@graph']);
        if (response.data['@context']) commit('SET_CONTEXT', response.data['@context']);

        commit('SET_VIOLATIONS_SUCCESS', false);
      } catch (error) {
        const errorMsg = getDetailedNetworkErrorMsg(error);
        commit('SET_REQUEST_ERROR', errorMsg || error);
      }
      commit('SET_LOADING', false);
    },

    async getViolationsBySchema({ dispatch }, { schema, type, shapeModel }) {
      dispatch('getViolations', { body: schema, bySchema: true, type, shapeModel });
    },
    async setShapeModel({ commit }, value) {
      commit('SET_SHAPE_MODEL', value);
    }
  },

  mutations: {
    SET_VIOLATIONS(state, payload) {
      state.violations = payload;
    },
    SET_CONTEXT(state, payload) {
      state.violationsContext = payload;
    },
    SET_VIOLATIONS_SUCCESS(state, payload) {
      state.violationsSuccess = !!payload;
    },
    SET_LOADING(state, payload) {
      state.isLoading = !!payload;
    },
    SET_REQUEST_ERROR(state, payload) {
      state.requestError = payload;
    },
    SET_SHAPE_MODEL(state, payload) {
      state.shapeModel = payload
    }
  },
};
