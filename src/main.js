import Vue from 'vue';
import VueMeta from 'vue-meta';
import UniversalPiwik from '@piveau/piveau-universal-piwik';
import router from './router';
import App from './App.vue';
import store from './store';
import vuetify from './plugins/vuetify';
import RuntimeConfiguration from './plugins/runtimeconfig';
import CorsproxyService from './plugins/corsproxy-service';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
library.add([
  faChevronDown
]);

/**********************************************
 *  Integrating the EC component library here *
 **********************************************/
require('@ecl/preset-ec/dist/styles/ecl-ec.css')

// Must be loaded for linting to work
window.jsyaml = require('js-yaml');
window.jsonlint = require('jsonlint-mod');

Vue.config.productionTip = false;

require('bootstrap');
require('bootstrap/scss/bootstrap.scss');
require('./style/custom-styles.css');

Vue.use(RuntimeConfiguration, { baseConfig: process.env, debug: process.env.NODE_ENV === 'development' });
Vue.use(CorsproxyService, Vue.prototype.$env.VUE_APP_CORSPROXY_API_URL);
Vue.use(UniversalPiwik, {
  router,
  isPiwikPro: Vue.prototype.$env.VUE_APP_TRACKER_IS_PIWIK_PRO,
  trackerUrl: Vue.prototype.$env.VUE_APP_TRACKER_TRACKER_URL,
  siteId: Vue.prototype.$env.VUE_APP_TRACKER_SITE_ID,
  pageViewOptions: { onlyTrackWithLocale: false, delay: 500 },
  debug: process.env.NODE_ENV === 'development',
  verbose: false,
});

Vue.use(VueMeta);

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
